# Example

Here you find an example texture atlas/sprite sheet generated with the following call:

```sh
$ SpriteSheetPacker -v --test 133 -o example --cpp-header

```

Both XML and JSON sheet files were available. Also a C++ header that defines the subtexture ids for easier access. You can also look up the subtexture by name.

## Todo

1) Loader example
