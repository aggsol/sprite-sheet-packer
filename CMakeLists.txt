cmake_minimum_required(VERSION 3.6)

project(SpriteSheetPacker LANGUAGES CXX VERSION 1.0.3)

# Set version information
configure_file(
  "${PROJECT_SOURCE_DIR}/src/Version.hpp.in"
  "${PROJECT_BINARY_DIR}/Version.hpp"
)
include_directories(${PROJECT_BINARY_DIR})

# copy binaries to root build dir
SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}")

# Create compile_commands.json for cppcheck
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
add_custom_target(cppcheck
    COMMAND cppcheck --project=./compile_commands.json
    WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
)

add_subdirectory(src)