# Sprite Sheet Packer 1.0.3

Small project to pack sprites or textures into a sprite sheet/texture atlas. Subtexture data is stored in JSON or XML files. Based on [TexturePacker](https://github.com/NicolasPerdu/TexturePacker) by [Nicolas Perdu](http://www.glusoft.com/tuto/how-to-make-sprite-sheets-generator.php).

![Example atlas texture](example/example.png)

## Usage

A folder of PNG files if packed onto a single texture. The texel locations of the sprites a re stored in an additional JSON or XML file. Example:

```sh
$ SpriteSheetPacker -i /path/to/images
```

will create two files `atlas.png` and `atlas.json`

### Command Line Parameters

| Parameter                 | Description                                                       |
| ---                       | ---                                                               |
|    -b, --bg-color <RGBA>  | Background color of the texture, default: 00000000 (transparent)  |
|    -i, --input <FOLDER>   | Folder containing PNG files                                       |
|    -o, --output <NAME>    | Name of the texture atlas files, default: atlas                   |
|    -s, --size <NUMBER>    | Size of square result texture, default 512                        |
|        --test <NUMBER>    | Generate a test atlas with the given number as random seed        |
|    -a, --allow-rotation   | Allow rotating sprites by 90 degrees                              |
|    -c, --cpp-header       | Create C++ header with subtexture id defines                      |
|    -p, --preview          | Show packed texture in preview window                             |
|    -v, --verbose          | Verbose mode                                                      |
|    -x, --xml              | Create XML file instead of JSON                                   |
|    -h, --help             | Show this help                                                    |
|        --version          | Print version                                                     |

### Sheet Data Examples

```json
{
  "TextureAtlas": {
    "imagePath": "atlas",
    "SubTexture": [
      {
        "name": "penguin.png",
        "x": 0,
        "y": 0,
        "width": 136,
        "height": 136,
        "rotation": 0,
        "id": 721482282
      },
      {
        "name": "pattern09.png",
        "x": 0,
        "y": 136,
        "width": 256,
        "height": 256,
        "rotation": 90,
        "id": 994583
      }
    ]
  }
}
```

```xml
<TextureAtlas imagePath="army">
    <SubTexture name="tank.png" x="0" y="0" width="24" height="56" rotation="90" id="3982342" />
    <SubTexture name="plane.png" x="0" y="100" width="24" height="56" rotation="0" id="99834872" />
</TextureAtlas>
```

## Dependencies

* [SFML](http://www.sfml-dev.org/) >= 2.3
* [CMake](https://cmake.org/) >= 3.6
* C++14 compliant compiler

## License

* Sprite Sheet Packer is [MIT](https://opensource.org/licenses/MIT) licensed
* [RectangleBinPack](https://github.com/juj/RectangleBinPack) by Jukka Jylänki is Public Domain
* [Crc32](http://create.stephan-brumme.com/crc32/) by Stephan Brumme has a zlib-style license

