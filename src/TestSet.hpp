#ifndef TEST_SET_HPP
#define TEST_SET_HPP

#include <SFML/Graphics.hpp>
#include <sstream>
#include <cstdlib>
#include <string>
#include <vector>
#include <memory>

using TexPtr = std::shared_ptr<sf::Texture>;

bool createTestSet(std::vector<std::string>& outTextNames, std::vector<TexPtr>& outTextures,
    unsigned testSet, bool verbose);

#endif