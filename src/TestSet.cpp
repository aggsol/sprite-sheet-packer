#include "TestSet.hpp"

#include <cstdint>
#include <iostream>

namespace
{

// Based on: https://stackoverflow.com/questions/43044/algorithm-to-randomly-generate-an-aesthetically-pleasing-color-palette
sf::Color createRandomColor(const sf::Color* mix)
{
    sf::Color color;

    color.r = static_cast<uint8_t>(std::rand());
    color.g = static_cast<uint8_t>(std::rand());
    color.b = static_cast<uint8_t>(std::rand());

    if(mix != nullptr)
    {
        color.r = (color.r + mix->r) / 2;
        color.g = (color.g + mix->g) / 2;
        color.b = (color.b + mix->b) / 2;
    }

    return color;
}
}

bool createTestSet(std::vector<std::string>& outTextNames, std::vector<TexPtr>& outTextures,
    unsigned testSet, bool verbose)
{
    std::srand(testSet); // Reproducible test sets


    unsigned count = static_cast<unsigned>(std::rand()) % 64;
    if(count < 16)
    {
        count = 16;
    }

    if(verbose)
    {
        std::cout << "Create " << count << " test images\n";
    }

    for(unsigned i=0; i<count; ++i)
    {
        auto color = createRandomColor(&sf::Color::White);
        sf::Vector2u size;
        size.x = 16 + (static_cast<unsigned>(std::rand()) % 128);
        size.y = 16 + (static_cast<unsigned>(std::rand()) % 128);

        sf::Image image;
        image.create(size.x, size.y, color);

        auto texture = std::make_shared<sf::Texture>();
        if(not texture->loadFromImage(image))
        {
            std::cerr << "Failed to create test image\n";
            return false;
        }

        outTextures.push_back(texture);
        std::ostringstream name;
        name << "img" << i;
        outTextNames.push_back(name.str());
    }

    return true;
}
