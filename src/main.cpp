#include "CliArguments.hpp"
#include "AtlasData.hpp"
#include "MaxRectsBinPack.hpp"
#include "TestSet.hpp"
#include "Version.hpp"

#include <cstdint>
#include <dirent.h>
#include <fstream>
#include <iostream>
#include <memory>
#include <SFML/Graphics.hpp>
#include <sstream>

using TexPtr = std::shared_ptr<sf::Texture>;

namespace
{

void usage()
{
    std::cout <<
R"(Usage: SpriteSheetPacker [OPTIONS] [FLAGS]
Pack the PNG files from the input folder into a texture atlas.

Options:
    -b, --bg-color <RGBA>   Background color of the texture, default: 00000000 (transparent)
    -i, --input <FOLDER>    Folder containing PNG files
    -o, --output <NAME>     Name of the texture atlas files, default: atlas
    -s, --size <NUMBER>     Size of square result texture, default: 512
        --test <NUMBER>     Generate a test atlas with the given number as random seed

Flags:
    -a, --allow-rotation    Allow rotating sprites in 90 degrees
    -c, --cpp-header        Create C++ header with subtexture id defines
    -p, --preview           Show packed texture in preview window
    -v, --verbose           Verbose mode
    -x, --xml               Create XML file instead of JSON

    -h, --help              Show this help
        --version           Print version
)";
}

// Returns the PNG file names in the given path
std::vector<std::string> getPngFiles(const std::string& path)
{
    std::vector<std::string> result;

    auto dir = opendir(path.c_str());
    if(dir == nullptr)
    {
        std::cerr << "Invalid folder: " << path << "\n";
        exit(EXIT_FAILURE);
    }

    std::string name;
    std::string ext;
    auto entry = readdir(dir);
    while(entry != nullptr)
    {
        name = entry->d_name;
        auto last = name.find_last_of(".");
        if(last != std::string::npos)
        {
            ext = name.substr(last);

            if(ext == ".png" || name == ".PNG")
            {
                result.push_back(name);
            }
        }
        entry = readdir(dir);
    }
    closedir(dir);
    dir = nullptr;

    return result;
}

// Test run all heuristics to select the best
rbp::MaxRectsBinPack::FreeRectChoiceHeuristic chooseBestHeuristic(const std::vector<TexPtr>& rects,
    size_t texWidth, size_t texHeight, bool verbose, bool allowRotation)
{
    rbp::MaxRectsBinPack pack;
    std::vector<rbp::MaxRectsBinPack::FreeRectChoiceHeuristic> listHeuristics;
    listHeuristics.push_back(rbp::MaxRectsBinPack::RectBestAreaFit);
    listHeuristics.push_back(rbp::MaxRectsBinPack::RectBestLongSideFit);
    listHeuristics.push_back(rbp::MaxRectsBinPack::RectBestShortSideFit);
    listHeuristics.push_back(rbp::MaxRectsBinPack::RectBottomLeftRule);
    listHeuristics.push_back(rbp::MaxRectsBinPack::RectContactPointRule);

    rbp::MaxRectsBinPack::FreeRectChoiceHeuristic res;
    float max = 0;

    for (auto& heu : listHeuristics)
    {
        pack.Init(texWidth, texHeight, allowRotation);

        for (size_t j = 0; j < rects.size(); j++)
        {
            pack.Insert(rects.at(j)->getSize().x, rects.at(j)->getSize().y, heu);
        }

        auto occupancy = pack.Occupancy();

        if(verbose)
        {
            std::cout << "Heuristic: " << heu << " Occupancy: " << occupancy << "%\n";
        }

        if (occupancy > max) {
            max = pack.Occupancy();
            res = heu;
        }
    }
    return res;
}

bool loadTextures(std::vector<std::string>& outTextNames, std::vector<TexPtr>& outTextures,
    const std::string inputPath, bool verbose)
{
    std::vector<std::string> pngFiles = getPngFiles(inputPath);

    if(verbose)
    {
        std::cout << "Found following files:\n";
        for(auto& f: pngFiles)
        {
            std::cout <<"- " << f << "\n";
        }
    }

    if(pngFiles.size() == 0)
    {
        std::cerr << "No PNG files found in folder: " << inputPath << "\n";
        return false;
    }

    for (auto& img : pngFiles)
    {
        auto pTex = std::make_shared<sf::Texture>();
        if(pTex->loadFromFile(inputPath + img))
        {
            outTextures.push_back(pTex);
            outTextNames.push_back(img);
        }
        else
        {
            std::cerr << "Cannot load file: " << img << "\n";
        }
    }

    if(outTextures.size() == 0)
    {
        std::cerr << "No valid images found in folder: " << inputPath << "\n";
        return false;
    }

    return true;
}

}

int main(int argc, const char* const argv[])
{
    bodhi::CliArguments args(argc, argv);

    // Option values
    auto dim        = args.getOpt<uint16_t>("s", "size", 512);
    auto filename   = args.getOpt<std::string>("o", "output", "atlas");
    auto input      = args.getOpt<std::string>("i", "input", "");
    auto testSet    = args.getOpt<unsigned>("", "test", 0);
    auto rgba       = args.getOpt<std::string>("b", "bg-color", "ff00ffff");
    uint32_t bgColor;
    std::stringstream ss;
    ss << std::hex << rgba; ss >> bgColor;

    // Bool flags
    auto allowRotation  = args.getOpt("a", "allow-rotation");
    auto cppHeader      = args.getOpt("c", "cpp-header");
    auto createXml      = args.getOpt("x", "xml");
    auto help           = args.getOpt("h", "help");
    auto preview        = args.getOpt("p", "preview");
    auto verbose        = args.getOpt("v", "verbose");
    auto version        = args.getOpt("", "version");

    if(args.arguments().size() > 0)
    {
        std::cerr << "Unknown parameters: ";
        for(auto a: args.arguments())
        {
            std::cerr << a << " ";
        }

        std::cerr << "\n";
        return 109;
    }

    if(version)
    {
        std::cout << VERSION_MAJOR << "." << VERSION_MINOR << "." << VERSION_PATCH << "\n";
        return 0;
    }

    if(help || (input.empty() && testSet == 0))
    {
        usage();
        return 0;
    }

    if(dim > sf::Texture::getMaximumSize())
    {
        std::cerr << "Size too big. Maximum texture size: " << sf::Texture::getMaximumSize() << "\n";
        return 105;
    }

    if(input.back() != '/')
    {
        input += '/';
    }

    std::vector<TexPtr> textures;
    std::vector<std::string> textureNames;

    if(testSet == 0)
    {
        if(not loadTextures(textureNames, textures, input, verbose))
        {
            return 101;
        }
    }
    else
    {
        if(not createTestSet(textureNames, textures, testSet, verbose))
        {
            return 102;
        }
    }

    sf::Vector2i size(dim, dim); // size of the sprite sheet
    rbp::MaxRectsBinPack pack(size.x, size.y, allowRotation); //pack for the atlas

    float rotation = 0;
    const auto best = chooseBestHeuristic(textures, size.x, size.y, verbose, allowRotation);

    sf::Color background(bgColor);
    sf::RenderTexture target; // texture to render the sprite sheet
    if(not target.create(size.x, size.y))
    {
        std::cerr << "Cannot create target texture (" << size.x << ", " << size.y << ")\n";
        return 102;
    }
    target.clear(background);

    std::vector<AtlasData> atlasData; // data for JSON/XML export
    for (size_t i = 0; i < textures.size(); i++)
    {
        rbp::Rect packedRect = pack.Insert(textures[i]->getSize().x, textures[i]->getSize().y, best);

        if (packedRect.height <= 0)
        {
            std::cerr << "Cannot pack all images onto texture.\nTry a larger size or less sprites.\nSee '--help' for details.\n";
            return 105;
        }

        sf::Sprite spr(*textures[i]); // sprite to draw on the rendertexture

        // if the image is rotated
        if (textures[i]->getSize().x == static_cast<unsigned>(packedRect.height)
                && packedRect.width != packedRect.height)
        {
            rotation = 90; // set the rotation

            // rotate the sprite to draw
            size_t oldHeight = spr.getTextureRect().height;
            spr.setPosition(static_cast<float>(packedRect.x), static_cast<float>(packedRect.y));

            spr.rotate(rotation);
            spr.setPosition(spr.getPosition().x + oldHeight, spr.getPosition().y);
        }
        else
        { // if there is no rotation
            rotation = 0;
            spr.setPosition(static_cast<float>(packedRect.x), static_cast<float>(packedRect.y));
        }

        target.draw(spr); // draw the sprite on the sprite sheet

        // save data of the image for the JSON/XML file
        atlasData.push_back(
            AtlasData(textureNames[i], packedRect.x, packedRect.y, packedRect.width,
                      packedRect.height, static_cast<size_t>(rotation) )
        );
    }

    target.display(); // render the texture

    // save the sprite sheet
    sf::Texture tex = target.getTexture();
    sf::Image img = tex.copyToImage();
    img.saveToFile(filename + ".png");

    // Save sheet data file
    std::ofstream ofs;
    if(createXml)
    {
        ofs.open(filename + ".xml");
        if(ofs.is_open())
        {
            AtlasData::createXmlSheet(ofs, filename, atlasData);
        }
        else
        {
            std::cerr << "Cannot write Xml file.\n";
        }
    }
    else
    {
        ofs.open(filename + ".json");
        if(ofs.is_open())
        {
            AtlasData::createJsonSheet(ofs, filename, atlasData);
        }
        else
        {
            std::cerr << "Cannot write Json file.\n";
        }
    }

    // create header file
    if(cppHeader)
    {
        std::ofstream header(filename + ".hpp");
        if(header.is_open())
        {
            AtlasData::createCppHeader(header, filename, atlasData);
        }
        else
        {
            std::cerr << "Cannot write C++ header file.\n";
        }
    }

    std::ostringstream result;
    if(verbose)
    {
        std::cout << "Background color r=" << static_cast<int>(background.r)
            << " g=" << static_cast<int>(background.g)
            << " b=" << static_cast<int>(background.b)
            << " a=" << static_cast<int>(background.a)
            << " ("  << rgba << ", " << bgColor << ")\n";
        result << "Packing: " << pack.Occupancy() << "%";
        std::cout << result.str() << "\n";
    }

    if(preview)
    {
        // SFML code the create a window and diplay the sprite sheet
        const auto desktopMode = sf::VideoMode::getDesktopMode();
        sf::VideoMode targetMode(size.x, size.y);

        sf::Sprite spr(tex);
        if(desktopMode < targetMode)
        {
            targetMode.width /= 2;
            targetMode.height /= 2;
            spr.setScale(0.5f, 0.5f);
        }

        sf::RenderWindow window(targetMode, result.str(), sf::Style::Titlebar | sf::Style::Close );

        while (window.isOpen())
        {
            sf::Event event;
            while (window.pollEvent(event))
            {
                if (event.type == sf::Event::Closed)
                {
                    window.close();
                }
                else if(event.type == sf::Event::KeyPressed)
                {
                    if(event.key.code == sf::Keyboard::Escape)
                    {
                        window.close();
                    }
                }
            }

            window.clear(background);
            window.draw(spr);
            window.display();
        }
    }
    return 0;
}
