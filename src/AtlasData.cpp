#include "AtlasData.hpp"

#include <algorithm>
#include <cassert>
#include <iomanip>
#include <iostream>
#include <string>

namespace
{
    // Crc32.cpp
    // Copyright (c) 2011-2016 Stephan Brumme. All rights reserved.
    // see http://create.stephan-brumme.com/disclaimer.html
    //
    /// compute CRC32 (half-byte algorithm)
    uint32_t crc32(const void* data, size_t length, uint32_t previousCrc32)
    {
        uint32_t crc = ~previousCrc32; // same as previousCrc32 ^ 0xFFFFFFFF
        //   const uint8_t* current = (const uint8_t*) data;
        const uint8_t* current = static_cast<const uint8_t*>(data);

        /// look-up table for half-byte, same as crc32Lookup[0][16*i]
        static const uint32_t Crc32Lookup16[16] =
        {
        0x00000000,0x1DB71064,0x3B6E20C8,0x26D930AC,0x76DC4190,0x6B6B51F4,0x4DB26158,0x5005713C,
        0xEDB88320,0xF00F9344,0xD6D6A3E8,0xCB61B38C,0x9B64C2B0,0x86D3D2D4,0xA00AE278,0xBDBDF21C
        };

        while (length-- != 0)
        {
        crc = Crc32Lookup16[(crc ^  *current      ) & 0x0F] ^ (crc >> 4);
        crc = Crc32Lookup16[(crc ^ (*current >> 4)) & 0x0F] ^ (crc >> 4);
        current++;
        }

        return ~crc; // same as crc ^ 0xFFFFFFFF
    }
}

AtlasData::AtlasData(const std::string& name, size_t tx, size_t ty, size_t tw, size_t th, size_t rot)
: m_name(name)
, m_x(tx)
, m_y(ty)
, m_width(tw)
, m_height(th)
, m_rotation(rot)
{
    assert(not name.empty());
    m_crc32 = crc32(m_name.c_str(), m_name.size(), 0);
}

std::ostream& AtlasData::createXmlSheet(std::ostream& strm, const std::string& name,
    const std::vector<AtlasData>& data)
{
    strm << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
         << "<TextureAtlas imagePath=\"" << name << "\">\n";

    for(const auto& i: data)
    {
        strm << "    ";
        i.toXml(strm);
    }

    strm << "</TextureAtlas>\n";
    return strm;
}

std::ostream& AtlasData::toXml(std::ostream& strm) const
{
    strm << "<SubTexture name=\"" << m_name << "\" x=\"" << m_x << "\" y=\"" << m_y
         << "\" width=\"" << m_width << "\" height=\"" << m_height
         << "\" rotation=\"" << m_rotation
         << "\" id=\"" << m_crc32
         << "\" />\n";

    return strm;
}

std::ostream& AtlasData::createJsonSheet(std::ostream& strm, const std::string& name,
    const std::vector<AtlasData>& data)
{
    strm << "{\n"
         << "    \"TextureAtlas\":\n"
         << "    {\n"
         << "        \"imagePath\": \"" << name << "\",\n"
         << "        \"SubTexture\":\n"
         << "        [\n";

    bool first = true;
    for(const auto& i: data)
    {
        if(not first)
        {
            strm << ",\n";
        }
        strm << "            ";
        i.toJson(strm);
        first = false;
    }
    strm << "\n        ]\n"
         << "    }\n"
         << "}\n";
    return strm;
}

std::ostream& AtlasData::toJson(std::ostream& strm) const
{
    strm << "{ \"name\": \"" << m_name << "\", \"x\": " << m_x << ", \"y\": " << m_y
        << ", \"width\": " << m_width << ", \"height\": " << m_height
        << ", \"rotation\": " << m_rotation
        << ", \"id\": " << m_crc32
        << " }";

    return strm;
}

std::ostream& AtlasData::createCppHeader(std::ostream& strm, const std::string& name,
        const std::vector<AtlasData>& data)
{
    std::string upper = name;
    std::transform(name.begin(), name.end(), upper.begin(), ::toupper);
    strm << "// DO NOT EDIT BY HAND! -- Generated automatically by SpriteSheetPacker\n\n"
         << "#ifndef SSP_" << upper << "_HPP\n"
         << "#define SSP_" << upper << "_HPP\n\n";

    for(const auto& d: data)
    {
        // TODO: properly sanitize filename
        upper = d.m_name;
        std::transform(d.m_name.begin(), d.m_name.end(), upper.begin(), ::toupper);
        
        std::replace(upper.begin(), upper.end(), '.', '_');
        std::replace(upper.begin(), upper.end(), '-', '_');

        strm << "#define SSP_" << upper
             << " 0x"
             << std::setfill ('0') << std::setw(8)
             << std::hex << d.m_crc32 << "\n";
    }

    strm << "\n#endif\n\n";
    return strm;
}
