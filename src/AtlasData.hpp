#ifndef ATLAS_DATA_HPP
#define ATLAS_DATA_HPP

#include <cstdint>
#include <string>
#include <ostream>
#include <vector>

// Atlas data for the JSON/XML sheet file und C++ header
class AtlasData
{
public:
    AtlasData(const std::string& name, size_t tx, size_t ty, size_t tw, size_t th, size_t r);
    AtlasData() = delete;

    // XML
    static std::ostream& createXmlSheet(std::ostream& strm, const std::string& name,
        const std::vector<AtlasData>& data);

    std::ostream& toXml(std::ostream& strm) const;

    // JSON
    static std::ostream& createJsonSheet(std::ostream& strm, const std::string& name,
        const std::vector<AtlasData>& data);

    std::ostream& toJson(std::ostream& strm) const;

    // cpp header
    static std::ostream& createCppHeader(std::ostream& strm, const std::string& name,
        const std::vector<AtlasData>& data);

private:
    std::string m_name;
    uint32_t    m_crc32;

    size_t      m_x;
    size_t      m_y;
    size_t      m_width;
    size_t      m_height;
    size_t      m_rotation;

};

#endif
